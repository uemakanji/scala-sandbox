object PatternMatchType {
  def main(args: Array[String]): Unit = {
    val list = List(1, true, "J")

    for (value <- list) {
      value match {
        case num: Int => println(s"num = ${num}")
        case bool: Boolean => println(s"bool = ${bool}")
        case name: String => println(s"name = ${name}")
        case _ => println(s"value = ${value}")
      }
    }
  }
}
