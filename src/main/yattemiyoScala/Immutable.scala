object Immutable {
  def main(args: Array[String]): Unit = {
    val immutable = "Immutable"
    println(immutable)

    immutable = "Change imutable"
    println(immutable)
  }
}
