object PatternMatchVar {
  def main(args: Array[String]): Unit = {
    val random = 4

    val result = random match {
      case num => num * 2
    }

    println(s"random: ${random}")
    println(s"number: ${result}")
  }
}
