object CaseClass {
  def main(args: Array[String]): Unit = {
    val domain1 = Domain3(1, "冨樫")
    val domain2 = Domain3(1, "冨樫")

    println(domain1 == domain2)
    println(domain1 != domain2)

    val domain3 = Domain3(2, "虎丸")

    println(domain1 == domain3)
    println(domain1 != domain3)
  }
}

case class Domain3(val id: Long, val name: String)