object FunctionExample {
  def main(args: Array[String]): Unit = {
    var calculate = (x: Int) => x + 5

    printf("y = %s\n", calculate(1))
    printf("y = %s\n", calculate(2))
  }
}