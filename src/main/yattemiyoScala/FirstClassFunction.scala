object FirstClassFunction {
  def main(args: Array[String]): Unit = {
    val add = (x: Int) => {
      val _add = (y: Int) => y + 5
      _add(x)
    }

    println(s"add = ${ add(1) }")
    println(s"add = ${ add(2) }")
    println(s"add = ${ add(1) }")
  }
}
