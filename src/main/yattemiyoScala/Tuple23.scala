object Tuple23 {
  def get() = {
    (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22)
    //(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23) //23以上はない
  }

  def main(args: Array[String]): Unit = {
    val tuple = get()
    println(tuple._1)
    println(tuple._22)
    //println(tuple._23) //23以上はない
  }
}
