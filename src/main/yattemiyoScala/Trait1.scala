object Trait1 {
  def main(args: Array[String]): Unit = {
    val man = new Man
    man.sayName()
  }
}

trait Human {
  val name = "飛行帽"

  def sayName(): Unit = {
    println(s"My name is $name")
  }
}

class Man extends Human