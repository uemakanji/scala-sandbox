object PatternMatchNoWild {
  def main(args: Array[String]): Unit = {
    val list = List(1, true, "J")

    for (value <- list) {
      value match {
        case num: Int => println(s"num = ${num}")
        case bool: Boolean => println(s"bool = ${bool}")
      }
    }
  }
}
