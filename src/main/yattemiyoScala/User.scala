class User(val id: Long, val firstname: String, val lastname: String, val fullname: Fullname = new Fullname) {
  def getFullname() = this.fullname.get(firstname, lastname)
}

class Fullname() {
  def get(firstname: String, lastname: String) = s"${firstname} ${lastname}"
}

class FullnameInJapan() extends Fullname {
  override def get(firstname: String, lastname: String) = s"${lastname} ${firstname}"
}

val user1 = new User(1, "kanji", "uema", new FullnameInJapan)
println(user1.getFullname())

val user2 = new User(2, "takeru", "sato")
println(user2.getFullname())