object Option1 {
  def main(args: Array[String]): Unit = {
    val map = Map(1 -> "M", 2 -> "L", 3 -> "H")

    def check(o: Option[String]) = {
      o match {
        case Some(s) => println(s)
        case None => println("none")
      }
    }

    val some = map.get(2)
    val none = map.get(4)

    check(some)
    check(none)
  }
}
