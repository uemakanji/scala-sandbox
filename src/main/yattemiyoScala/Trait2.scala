object Trait2 {
  def main(args: Array[String]): Unit = {
    val man = new Man("虎丸", "男塾", "１号生")
    man.say()
  }
}

trait Human {
  val name: String
}

trait Job {
  val job: String
}

trait Grade {
  val grade: String
}

class Man(n: String, j: String, g: String) extends Human with Job with Grade {
  val name = n
  val job = j
  val grade = g

  def say(): Unit = {
    println(s"名前は${this.name}じゃ。${this.job}${this.grade}じゃ。")
  }
}
