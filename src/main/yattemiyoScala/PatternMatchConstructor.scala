object PatternMatchConstructor {
  case class Person(val name: String, val age: Int, val education: String)

  def main(args: Array[String]): Unit = {
    val person = Person("椿山 清美", 15, "男塾")

    val belongTo = person match {
      case Person("独眼鉄", _, "男塾") => "鎮守直廊三人衆"
      case Person("椿山 清美", _, "男塾") => "一号生"
      case _ => "一般市民"
    }

    println(s"所属: ${belongTo}")
  }
}
