object Mutable {
  def main(args: Array[String]): Unit = {
    var mutable = "Mutable"
    println(mutable)

    mutable = "Change mutable"
    println(mutable)
  }
}
