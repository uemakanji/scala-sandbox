object CaseClassTuple {
  def get() = {
    val name = new Name("uema", "kanji")
    (Human(name, 15), name)
  }

  def main(args: Array[String]): Unit = {
    val tuple = get()
    println(tuple._1.name.fullname)
    println(tuple._2.fullname)
  }
}

class Name(first: String, last: String) {
  def fullname() = s"$first $last"
}

case class Human(name: Name, age: Int)