object PatternMatchSeq {
  def main(args: Array[String]): Unit = {
    val seq = Seq(1, 2, 3, 4)

    val result1 = seq match {
      case Seq(1, a, _*) => a
      case _ => 0
    }

    val result2 = seq match {
      case Seq(a, 2, _*) => a
      case _ => 0
    }

    val result3 = seq match {
      case Seq(a, 1, _*) => a
      case _  => 0
    }

    println(s"number1: ${result1}")
    println(s"number2: ${result2}")
    println(s"number3: ${result3}")
  }
}
