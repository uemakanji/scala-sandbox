object NoneSideEffect {
  def main(args: Array[String]): Unit = {
    var add = (x: Int) => x + 5

    println(s"add = ${ add(1) }")
    println(s"add = ${ add(1) }")
    println(s"add = ${ add(1) }")
  }
}
