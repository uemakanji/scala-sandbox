object SideEffect {
  var total  = 0

  def main(args: Array[String]): Unit = {
    val add = (x: Int) => {
      total += x
      total
    }

    //副作用がある
    println(s"total = ${ add(1) }")
    println(s"total = ${ add(1) }")
    println(s"total = ${ add(1) }")
  }
}
