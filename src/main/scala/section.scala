object section {
  def f(g: Int => Int) = g(5)

  def main(args: Array[String]): Unit = {
    println((f(x => 2 + x), f(2 + _)))
    println((f(x => x + 2), f(_ + 2)))
    println((f(x => 2 - x), f(2 - _)))
    println((f(x => x - 2), f(_ - 2)))
  }
}
