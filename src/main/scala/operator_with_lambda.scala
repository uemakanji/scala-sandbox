object operator_with_lambda {
  def f1(g: Int => Int) = g(1)
  def f2(g: (Int, Int) => Int) = g(2, 3)

  def main(args: Array[String]): Unit = {
    /*
    println(f1(x => x - 3))
    println(f1(x => 3 - x))
    println(f2((x, y) => x + y))
    */
    println(f1(_ - 3))
    println(f1(3 - _))
    println(f2(_ + _))
  }
}
