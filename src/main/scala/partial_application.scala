object partial_application {
  /*
  def add: Int => Int => Int = x => y => x + y
  val add2 = add(2)
  */
  def add(x: Int, y: Int): Int = x + y
  def add2 = add(2, _: Int)

  def main(args: Array[String]): Unit = {
    println(add(2,3))
    println(add2(3))
    println((add(2, _:Int))(3))
  }
}
