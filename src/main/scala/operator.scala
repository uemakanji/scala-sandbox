object operator {
  def f(g: (Int, Int, Int) => Int) = g(2, 3, 4)

  def main(args: Array[String]): Unit = {
    println(f(_ + _ + _))
    println(f(_ * _ + _))
  }
}
