object higher_order_with_variable {
  def f(g: (Int, Int) => Int) = {
    g(2, 3)
  }

  /*
  def add(x: Int, y: Int): Int = x + y
  val mul: (Int, Int) => Int = (x, y) => x * y
  */

  def main(args: Array[String]): Unit = {

    /*
    println(f(add))
    println(f(mul))
    */
    println(f((x, y) => x + y))
    println(f((x, y) => x * y))
  }
}
