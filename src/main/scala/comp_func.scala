object comp_func {
  def f(x: Int): Int = x + 1
  def g(x: Int): Int = x * 2

  def main(args: Array[String]): Unit = {
    println(f(g(1)))
    println((f _).compose(g)(1))
    println(g(f(1)))
    println((f _).andThen(g)(1))
  }
}
