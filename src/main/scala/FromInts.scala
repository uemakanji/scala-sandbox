object FromInts {
  trait FromInt[A] {
    def to(from: Int): A
  }

  object FromInt {
    implicit object FromInt extends FromInt[Int] {
      def to(from: Int): Int = from
    }

    implicit object FromIntToDouble extends FromInt[Double] {
      def to(from: Int): Double = from
    }
  }
}
