object def_around {
  def around(init: () => Unit, body: () => Any, fin: () => Any): Any = {
    init()
    try {
      body()
    } finally {
      fin()
    }
  }

  def main(args: Array[String]): Unit = {
    around(
      () => println("ファイルを開く"),
      () => println("ファイルを処理する"),
      () => println("ファイルを閉じる")
    )
  }
}