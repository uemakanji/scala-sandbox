object operator_partial_application {
  def f(g: Int => Int) = g(5)

  def main(args: Array[String]): Unit = {
    println(f(2 - _))
    println(f(2 * _))
  }
}
