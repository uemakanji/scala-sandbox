object curry {
  def add1(x: Int, y: Int) = x + y
  def add2(x: Int): Int => Int = y => x + y
  def add3: Int => Int => Int = x => y => x + y

  def main(args: Array[String]): Unit = {
    println(add1(2, 3))
    println(add2(2)(3))
    println(add3(2)(3))

  }
}
