object combine {
  //def combine[A](a: A, b: A, c: A) = a +: b +: List(c)

  def combine = (a: Any) => (b: Any) => (c: Any) => a +: b +: List(c)

  def main(args: Array[String]): Unit = {
    /*
    val a = combine(1, _: Int, _: Int)
    val b = a(2, _: Int)
    val c = b(3)
    println(c)
    println(combine('a', 'b', 'c'))
    */

    val a = combine(1)
    val b = a(2)
    val c = b(3)
    println(c)
    println(combine('a')('b')('c'))
  }
}
