import Nums._
import FromInts._

object Additive {
  def average[A](list: List[A])(implicit a: Num[A], b: FromInt[A]): A = {
    val length: Int = list.length
    val sum: A = list.foldLeft(a.zero)((x, y) => a.plus(x, y))
    a.divide(sum, b.to(length))
  }

  def median[A:Num:Ordering:FromInt](list: List[A]): A = {
    val num = implicitly[Num[A]]
    val ord = implicitly[Ordering[A]]
    val int = implicitly[FromInt[A]]
    val size = list.size
    require(size > 0)
    val sorted = list.sorted
    if (size % 2 == 1) {
      sorted(size / 2)
    } else {
      val fst = sorted((size / 2) - 1)
      val snd = sorted((size / 2))
      num.divide(num.plus(fst, snd), int.to(2))
    }
  }

  def main(args: Array[String]): Unit = {
    val aa = average(List(1, 3, 5))
    val bb = average(List(1.5, 2.5, 3.5))
    println(aa)
    println(bb)

    val cc = median(List(1, 3, 2))
    val dd = median(List(1.5, 2.5, 3.5))
    val ee = median(List(1, 3, 4, 5))
    println(cc)
    println(dd)
    println(ee)
  }
}

