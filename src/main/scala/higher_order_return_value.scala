object higher_order_return_value {
  def add(x: Int): Int => Int = y => x + y
  val add2 = add(2)

  def main(args: Array[String]): Unit = {
    println(add2(3))
    println((add(2))(3))
    println(add(2)(3))
  }
}
